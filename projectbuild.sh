#!/bin/bash

mkdir appRoot
cd appRoot && ng new angular7demo --defaults
cp -r -n /appRoot/angular7demo/ /gitRoot/
cd /gitRoot/angular7demo && npm install --prod && npm run setup
cd /gitRoot/angular7demo && ng build
cp -r -n /gitRoot/ /code/