#####################################################################
# Google Cloud Platform
#####################################################################
provider "google" {
  credentials = "${file("/code/gitRoot/terraform/account.json")}"
  project = "${var.project}"
  region  = "${var.region}"
}