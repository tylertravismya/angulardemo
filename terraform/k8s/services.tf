resource "kubernetes_service" "angular7-master" {
  metadata {
    name = "angular7-master"

    labels {
      app  = "angular7demo"
    }
  }

  spec {
    selector {
      app  = "angular7demo"
    }

    port {
      name        = "angular-port"
      port        = 8080
      target_port = 8080
    }

    port {
      name        = "mongoose-port"
      port        = 4000
      target_port = 4000
    }
    
    port {
      name        = "mongo-port"
      port        = 27017
      target_port = 27017
    }

    type = "LoadBalancer"
  }
  
}
