resource "kubernetes_replication_controller" "angular7demo-master" {
  metadata {
    name = "angular7demo-master"

    labels {
      app  = "angular7demo"
    }
  }

  spec {
    replicas = 1

    selector = {
      app  = "angular7demo"
    }

    template {
      container {
        image = "mongo"
        name  = "mongo"

        port {
          container_port = 27017
        }

        resources {
          requests {
            cpu    = "100m"
            memory = "100Mi"
          }
        }
      }
      
      container {
        image = "realmethods/angular7demo:latest"
        name  = "terraform-angular7demo-container"

        env {
          name  = "MONGOOSE_HOST_NAME"
          value = "${kubernetes_service.angular7-master.load_balancer_ingress.0.ip}"
                 
        }
        
 #       env {
 #         name  = "MONGO_HOST_ADDRESS"
 #         value = "mongodb://${kubernetes_service.angular7-master.load_balancer_ingress.0.ip}:27017/angular7demo"
 #       }
        
        port {
          container_port = 8080
        }
        
                
        port {
          container_port = 4000
        }

        resources {
          requests {
            cpu    = "100m"
            memory = "100Mi"
          }
        }
      }
    }
  }
}

